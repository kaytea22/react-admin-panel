import React from 'react';
import { Create, Edit, List, Datagrid, TextField, EditButton, Filter, TextInput, ReferenceInput, SelectInput, SimpleForm } from 'react-admin';

export const UserList = (props) => (
    <List {...props} perPage={25} title="All Users" >
        <Datagrid>
            <TextField label="Id" source="id" />
            <TextField label="First Name" source="firstName" />
            <TextField label="Last Name" source="lastName" />
            <TextField label="Email" source="email" />
            <TextField label="Company" source="companyName" />
            <TextField label="Role" source="role" />
            <EditButton />
        </Datagrid>
    </List>
);

export const UserCreate = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="first_name" value="first_name" />
            <TextInput source="last_name" value="last_name" />
            <TextInput source="email" value="email" />
            <TextInput source="password" value="password" />
        </SimpleForm>
    </Create>
);

const UserTitle = ({ record }) => {
    // eslint-disable-next-line
    return <span>{record ? `${record.firstName}` + ' ' + `${record.lastName}` : '' } </span>;
}

export const UserEdit = (props) => (
    <Edit title = { <UserTitle /> }{...props}>
        <SimpleForm>
            <TextInput source="firstName" value="first_name" />
            <TextInput source="lastName" value="lastName" />
            <TextInput source="email" value="email" />
            <TextInput source="password" value="password" />
        </SimpleForm>
    </Edit>
);

export const UserFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="q" alwaysOn />
        <ReferenceInput label="User" source="userId" reference="users" allowEmpty>
            <SelectInput optionText="name" />
        </ReferenceInput>
    </Filter>
);
