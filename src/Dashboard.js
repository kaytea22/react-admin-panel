import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

export default () => (
    <Card>
        <CardHeader title = "Welcome" />
        <CardContent>
            Number of new users for the past month. <br />
            Number of users currently logged in. <br />
            Number of users logged in the past 24 hours. <br />
            Number of users logged in the past week. <br />
            Number of users logged in the past month. <br />
        </CardContent>
    </Card>
);