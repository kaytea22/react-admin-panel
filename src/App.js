import React, { Component } from 'react';
import UserIcon from '@material-ui/icons/Group';
import Dashboard from './Dashboard';
import dataProvider from './dataProvider';
import AuthProvider from './AuthProvider';

import { Admin, Resource } from 'react-admin';
import { UserList, UserCreate, UserEdit } from './UserCRUD';



class App extends Component {

  render() {
    return (
      <Admin title = "FreeMaps Admin" dashboard = { Dashboard } dataProvider = { dataProvider } authProvider = { AuthProvider } >
        <Resource name = "Users" label = "Users"  list = { UserList } edit = { UserEdit } create = { UserCreate } icon = { UserIcon }  />
      </Admin>
    );
  }
}


export default App;
