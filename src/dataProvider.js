import {
    GET_LIST,
    GET_ONE,
    GET_MANY,
    GET_MANY_REFERENCE,
    CREATE,
    UPDATE,
    DELETE,
} from 'react-admin';
import { stringify } from 'query-string';
import axios from 'axios';
//  fetchUtils,


const API_URL = 'http://localhost:5001/api';

/**
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} { url, options } The HTTP request parameters
 */
const convertDataProviderRequestToHTTP = (type, resource, params) => {
    switch (type) {
        case GET_LIST: {
            resource = 'admin/GetUsers';
            const { page, perPage } = params.pagination;
            const { field, order } = params.sort;
            const query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([(page - 1) * perPage, page * perPage - 1]),
                filter: JSON.stringify(params.filter),
            };
            return {
                url: `${API_URL}/${resource}?${stringify(query)}`,
                options: { method: 'GET' },
            };
        }
        case GET_ONE: {
            resource = 'admin/GetSingleUser';
            return {
                url: `${API_URL}/${resource}`,
                options: { method: 'post', body: params.id },
            };
        }
        case GET_MANY: {
            resource = 'admin/GetUsers';
            const query = {
                filter: JSON.stringify({ id: params.ids }),
            };
            return { url: `${API_URL}/${resource}?${stringify(query)}` };
        }
        case GET_MANY_REFERENCE: {
            resource = 'admin/GetUsers';
            const { page, perPage } = params.pagination;
            const { field, order } = params.sort;
            const query = {
                sort: JSON.stringify([field, order]),
                range: JSON.stringify([(page - 1) * perPage, (page * perPage) - 1]),
                filter: JSON.stringify({ ...params.filter, [params.target]: params.id }),
            };
            return { url: `${API_URL}/${resource}?${stringify(query)}` };
        }
        case UPDATE:
            //id:
            //data: {Object}
            //previousData: {Object}
            resource = 'admin/UpdateUser';
            return {
                url: `${API_URL}/${resource}`,
                options: { method: 'post', body: params.data },
            };
        case CREATE:
            
            resource = 'register';
            return {
                url: `${API_URL}/${resource}`,
                options: { method: 'POST', body: params.data },
            };
        case DELETE:
            return {
                url: `${API_URL}/${resource}/${params.id}`,
                options: { method: 'DELETE' },
            };
        default:
            throw new Error(`Unsupported fetch action type ${type}`);
    }
};

/**
 * @param {Object} response HTTP response from fetch()
 * @param {String} type One of the constants appearing at the top if this file, e.g. 'UPDATE'
 * @param {String} resource Name of the resource to fetch, e.g. 'posts'
 * @param {Object} params The Data Provider request params, depending on the type
 * @returns {Object} Data Provider response
 */
const convertHTTPResponseToDataProvider = (response, type, params) => {
    const { data } = response;
    switch (type) {
        case GET_LIST:
            return {
                data: data,
                total: data.length,
            };
        case CREATE:
            return {
                data: { ...params.data, id: data.id }
            };
        case GET_ONE:
            return {
                data: data
            };
        default:
            return { data: data };
    }
};

/**
 * @param {string} type Request type, e.g GET_LIST
 * @param {string} resource Resource name, e.g. "posts"
 * @param {Object} payload Request parameters. Depends on the request type
 * @returns {Promise} the Promise for response
 */

//TODO: Use HTTPS 
export default (type, resource, params) => {
    const { url, options } = convertDataProviderRequestToHTTP(type, resource, params);
    let axiosBody = new FormData();
    if (type === "GET_ONE") {
        for (let property in options.body) {
            if (options.body.hasOwnProperty(property)) {
                axiosBody.set("id", options.body[property]);
            }
        }
    } else {
        for (let property in options.body) {
            if (options.body.hasOwnProperty(property)) {
                axiosBody.set(property, options.body[property]);
            }
        }
    }


    if (options.method === 'GET') {
        let x = axios({
            method: 'get',
            url: url,
        }).then(response => convertHTTPResponseToDataProvider(response, type, resource, params));
        return x;
    } else {
        let x = axios({
            method: 'post',
            url: url,
            config: { headers: { 'Content-Type': 'multipart/form-data' } },
            data: axiosBody
        }).then(response => convertHTTPResponseToDataProvider(response, type, resource, params));
        return x;
    }
};